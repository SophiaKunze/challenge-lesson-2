# Challenge Lesson 2 #

Mini Challenge for Today:
------------------------------
* Choose a great theme for VS Code (If you haven't yet)
* Create a single index.html file
* Link an external JavaScript file
* Make an empty UL list element and reference it in the JS file
* Make 2 inputs of type text in the HTML page and reference it in the JS file
* 1 Input for an emoji of the animal
* 1 Input for the name of the animal
* Create a render function to render out the animals
* Create a function constructor for an Animal
* should have id, name and emoji properties
* Make a "create animal" button in the HTML and reference it in the JS file
* Bind a event listener to the Button
* Clicking the button should read the text (The Value attribute) from the inputs
* Create a new animal using a Function Constructor (the new Keyword)
* Display the list of animals using the render function
* Helpful Links:
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
* https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button