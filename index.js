/**----------------------------------------------------
 * Functional Constructor
 ----------------------------------------------------*/
function Animal(id, name, emoji) {
    this.id = id
    this.name = name
    this.emoji = emoji
}

/**----------------------------------------------------
 * Variables
 ----------------------------------------------------*/
const animals = []

/**----------------------------------------------------
 * DOM ELEMENTS
 ----------------------------------------------------*/
// access empty list of animals
const animalListElement = document.getElementById("animals");
// access input emoji
const emojiInput = document.getElementById("input-emoji");
// access input id
const nameInput = document.getElementById("input-name");
// access button
const createAnimalButton = document.getElementById("create-animal-button");

/**----------------------------------------------------
 * Functions
 ----------------------------------------------------*/
function createId() {
    return Math.random().toString(16).slice(2);
}

function renderAnimals(animals) {
    // firstly clear the visualization in html
    animalListElement.innerHTML = "";
    // loop over every entry in list of animals
    for (const animal of animals) {
        animalListElement.innerHTML += `
            <li>${animal.name} ${animal.emoji}</li>
        `;
   }
}
/**----------------------------------------------------
 * Event Handlers
 ----------------------------------------------------*/
function addAnimalToList() {
    // get animal name and emoji from input
    const newAnimalName = nameInput.value//.trim();
    console.log(newAnimalName)
    const newAnimalEmoji = emojiInput.value//.trim();
    console.log(newAnimalEmoji)
    // generate id
    const newAnimalId = createId();
    // create new animal
    const newAnimal = new Animal(newAnimalId, newAnimalName, newAnimalEmoji);
    console.log(newAnimal)
    // add animal to list of animals 
    animals.push(newAnimal);
    // update visualization in html
    renderAnimals(animals);
}

// define what happens when button is clicked
createAnimalButton.addEventListener("click", addAnimalToList);